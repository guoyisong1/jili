/*
AE+ME 自动修改参数并添加到渲染队列的脚本
*/

{
    // 一些前置通用工作，如获取系统时间等
    function addLog(log) {
        var myFile = new File("D:\\AE_ExtendScript\\log.txt");
        myFile.open("a"); //a代表追加模式，若需覆盖文本请使用"w"模式
        myFile.writeln("dzh_log  " + gatTime() + "  " + log);
        myFile.close();
    }

    function gatTime() {
        var now = new Date();
        var hour = now.getHours();
        var minute = now.getMinutes();
        var second = now.getSeconds();
        var time = hour + ":" + minute + ":" + second;
        return time;
    }

    // 引用封装自定义的log函数等
    //@includepath "includepath_scripts"
    //@include "dzh_include_scripts.jsx"

    function exit(){
        alert("即将抛出异常强制停止脚本！");
        throw new Error('停止脚本执行'); // 抛出异常停止脚本
    }

    function ameRender(thisOutModule, thisItemNum)
    {
        // 启动AME 
        var bt = new BridgeTalk();
        if (!BridgeTalk.isRunning("ame")) {
            BridgeTalk.launch("ame", "background");
            alert("Launching Adobe Media Encoder");
        }
        thisOutModule.file = new File(dst_folder + compToRender + "_" + thisItemNum);
        // 加入AME队列，不开始渲染
        if (app.project.renderQueue.canQueueInAME == true) {
            app.project.renderQueue.queueInAME(false);
        } else {
            alert("There are no queued item in the Render Queue.");
        }
    }

    // 修改参数
    function changeHuakuai(k)
    {
        // 遍历工程中的所有合成
        for (var i = 1; i <= curProj.numItems; i++) {
            var curItem = curProj.item(i);
            // 找到名称为compToControl的合成
            if (curItem instanceof CompItem && curItem.name == compToControl) {
                // 遍历合成中的所有图层
                for (var j = 1; j <= curItem.numLayers; j++) {
                    var curLayer = curItem.layer(j);
                    // 找到空对象图层
                    if (curLayer instanceof AVLayer && curLayer.source.name === "控制层") {
                    // 更改滑块
                    curLayer.property("效果").property("总控制").property("滑块").setValue(k);
                    break;
                    }
                }
                break;
            }
        }
    }

    // 获取对应素材名称id
    function getFileName(k)
    {
        // 滑块k对应图层，k对应下面合成的k+1图层的名称
        // 合成名称：视频素材_多段拼接
        // 遍历工程中的所有合成
        for (var i = 1; i <= curProj.numItems; i++) {
            var curItem = curProj.item(i);
            if (curItem instanceof CompItem && curItem.name == compMaterial) {
                // 遍历合成中的所有图层
                var firstLayer = curItem.layer(k+1);
                var outputId = firstLayer.name;
                // addLog(outputId);
                return outputId;
            }
        }
        
    }
    
    // 重命名渲染模块输出文件
    function renameFile(outputModule, newName)
    {
        // 滑块k对应图层，
        outputModule.file = new File(dst_folder + "/" + newName);
        // addLog(outputModule.file.name);
    }


    // 设置变量
    var compToRender = "总合成"; // 合成名称
    var compToControl = "总合成"; // 合成名称-总控制所在的合成
    var sliderMax = 10; // 滑块最大值
    var sliderMin = 0; // 滑块初始值
    var sliderStep = 1; // 步长
    var compMaterial = "loopComp_视频素材"; // 读取素材名字的合成

    // 选择目标输出文件夹
    var defaultFolder = new Folder("D:/growth_creation/creation/20230916");
    var outputFolder = defaultFolder.selectDlg("请选择输出文件夹");
    var dst_folder = outputFolder.fsName;

    // 弹窗选择输出分辨率
    //  创建一个新的弹窗
    var dialog = new Window("dialog");
    //  设置弹窗标题
    dialog.text = "选择选项";
    //  创建一个组和文本描述
    var group = dialog.add("group");
    var description = group.add("statictext", undefined, "请选择输出分辨率");
    //  创建选项按钮
    var option1 = group.add("button", undefined, "1080p");
    var option2 = group.add("button", undefined, "720p");
    //  当用户点击选项按钮时，关闭弹窗并返回选项编号
    option1.onClick = function() {
        dialog.close(1);
    }
    option2.onClick = function() {
        dialog.close(2);
    }
    //  显示弹窗并等待用户选择选项
    var result = dialog.show();
    //  根据用户选择的结果执行操作
    if (result == 1) {
        compToRender = "总合成";
    } else if (result == 2) {
        compToRender = "总合成_720p";
    }


    //  启动AME 
    var bt = new BridgeTalk();
    if(!BridgeTalk.isRunning("ame"))
    {
        BridgeTalk.launch("ame", "background");
        alert("Launching Adobe Media Encoder");
    }

    // 获取当前工程
    var curProj = app.project;
    // 寻找目标合成并将合成加入渲染队列
    for (var i = 1; i <= curProj.numItems; i++) 
    {
        for (var i = 1; i <= curProj.numItems; i++) {
            var curItem = curProj.item(i);
            // 找到名称为compToRender的合成
            if (curItem instanceof CompItem && curItem.name == compToRender) {
                var renderQueue = curProj.renderQueue;
                if(renderQueue.numItems > 0){
                    alert("渲染队列不为空，请清空队列后再运行脚本！");
                    exit(); // 结束脚本
                }
                var newRender = renderQueue.items.add(curItem);
                var outputModule = newRender.outputModule(1);
            }
        }
    }
    if(!outputModule){
        alert("未找到目标渲染合成\""+compToRender+"\"!\n请检查名称设置!");
        exit(); // 结束脚本
    }

    // 修改参数，重命名，添加到AME队列
    var addQueueToAME = false
    for (var k = sliderMin; k <= sliderMax; k += sliderStep)
    {
        changeHuakuai(k);
        var newName = getFileName(k);
        renameFile(outputModule, newName);
        // 加入队列
        if (app.project.renderQueue.canQueueInAME === true)
        {
            app.project.renderQueue.queueInAME(false);
            // 已加入渲染队列的item取消渲染勾选
            // renderQueue.item(thisItemNum).render = false;
        } else {
            alert("There are no queued item in the Render Queue.");
        }
        // 第一次添加到队列时修改渲染设置
        if (addQueueToAME == false){
            addQueueToAME = true
            alert("请检查确认AME中渲染预设及输出路径！"
                + "\n点击【OK】按钮\n将以该渲染设置添加剩余队列"
                + "\n仅预设生效，自定义参数需保存成预设！");
        }

    }

    // app.project.renderQueue.render();   // 直接AE渲染

    alert("脚本执行完毕！");

}

